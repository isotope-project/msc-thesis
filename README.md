# isotope

This repository contains the LaTeX source for my MSc. thesis, which I completed under the supervision of Professor Sam Staton at Green Templeton College in the University of Oxford. The text is based on the old source for `isotope`, found at https://gitlab.com/isotope-project/old-isotope. Development of `isotope` is now continuing as part of my PhD at https://gitlab.com/isotope-project/isotope. Some other miscellaneous resources on `isotope` are also included.

A PDF version of the thesis may be accessed at https://isotope-project.gitlab.io/msc-thesis/thesis.pdf 