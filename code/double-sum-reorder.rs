fn double_sum_bad(v: Vec<u8>) -> (Vec<u8>, u64) {
    let s: &[u8] = slice_vec(&v);
    let u = double_vec(v);
    let h: u64 = sum(s);
    (u, h)
}