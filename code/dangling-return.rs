fn dangling_return(x: char) -> &char {
    &x
}

fn deref_dangling(x: char) -> char {
    *dangling_return(x)
}