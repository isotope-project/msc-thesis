#include<stdint.h>

int signed_overflow() {
    int32_t to_overflow = INT32_MAX;
    // Undefined behaviour: signed integer overflow!
    to_overflow++;
    return to_overflow;
}