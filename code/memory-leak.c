#include<stdlib.h>
#include<string.h>

void memory_leak() {
    char* memory = (char*)malloc(8192);
    strcpy(
        memory, 
        "This memory is malloc`ed, but never free`d;\n"
        "this is not incorrect, but does cause a memory leak!\n"
    );
}