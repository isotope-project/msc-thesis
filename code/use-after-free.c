#include<stdlib.h>

int use_after_free(int my_value) {
    int* ptr = (int*)malloc(sizeof(int));
    *ptr = my_value;
    free(ptr);
    // Undefined behaviour: dereferencing a freed pointer!
    return *ptr;
}