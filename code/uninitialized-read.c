int uninitialized_read() {
    int x;
    // Undefined behaviour: reading uninitialized memory!
    return x + 5;
}