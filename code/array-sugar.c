int array_sugared() {
    int array[5] = {1, 2, 3, 4, 5};
    return array[3];
}

int array_desugared() {
    int array[5] = {1, 2, 3, 4, 5};
    return *(array + 3);
}

int strange_sugared() {
    int array[5] = {1, 2, 3, 4, 5};
    return 3[array];
}

int strange_desugared() {
    int array[5] = {1, 2, 3, 4, 5};
    return *(3 + array);
}